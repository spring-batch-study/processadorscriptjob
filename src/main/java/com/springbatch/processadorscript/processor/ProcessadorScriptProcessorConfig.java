package com.springbatch.processadorscript.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.support.builder.ScriptItemProcessorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.springbatch.processadorscript.dominio.Cliente;

@Configuration
public class ProcessadorScriptProcessorConfig {

	/*
	Além de adicionar o arquivo em programa arguments:
	arquivoClientes=C:/Users/jbgar/Documents/projetos/processadorScriptJob/files/clientes.txt

	Deve-se também incluir na VM arguments:
	nashorn.args=scripting
	 */
	@Bean
	public ItemProcessor<Cliente, Cliente> processadorScriptProcessor() {
		return new ScriptItemProcessorBuilder<Cliente, Cliente>()
				.language("nashorn") // JS da JVM
				.scriptSource("var email = item.getEmail();" +
							"var arquivoExiste = `ls | grep ${email}.txt`;" +
//							"var arquivoExiste = `dir C:\\Users\\jbgar\\Documents\\projetos\\processadorScriptJob\\files | findstr ${email}.txt`;" +
							"if (!arquivoExiste) item; else null;")
				.build();
	}

}
